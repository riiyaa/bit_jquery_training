$(document).ready(function () {
    $("#hide").click(function () {
        $("h1").toggle();
    });
});


// GEt content
$(document).ready(function () {
    $("#text").click(function () {
        alert("Text: " + $("#get").text());
    });
    $("#html").click(function () {
        alert("HTML: " + $("#get").html());
    });
    $("#val").click(function () {
        alert("Value: " + $("#getval").val());
    });
});

// set content
$(document).ready(function () {
    $("#settext").click(function () {
        $("#test1").text("Hello world!");
    });
    $("#sethtml").click(function () {
        $("#test2").html("<b>Hello world!</b>");
    });
    $("#setvalue").click(function () {
        $("#test3").val("Dolly Duck");
    });
});

// add content
$(document).ready(function () {
    $("#addappend").click(function () {
        $(".append").append(" <b>Appended text</b>.");
    });
    $("#addappendol").click(function () {
        $("ol").append("<li>Appended item</li>");
    });


    $("#addprepend").click(function () {
        $(".append").prepend(" <b>Prepended text</b>.");
    });
    $("#addprependol").click(function () {
        $("ol").prepend("<li>Prepended item</li>");
    });
});

// remove content
$(document).ready(function () {
    $(".removebtn").click(function () {
        $("#div1").remove();
    });

    $(".emptybtn").click(function () {
        $("#div1").empty();
    });
});

// Manipulating CSS
$(document).ready(function () {
    $(".addClass").click(function () {
        $(".addClassp , .addClassh3 , .addClassh4").addClass("red");
        $(".addClassdiv").addClass("fontsize");
    });
    $(".removeClass").click(function () {
        $(".addClassh3, .addClassh4, .addClassp").removeClass("red");
        $(".addClassdiv").removeClass("fontsize");
    });
    $(".toggleClass").click(function () {
        $(".addClassh3, .addClassh4, .addClassp").toggleClass("red");
        $(".addClassdiv").toggleClass("fontsize");
    });
});

// Add css()
$(document).ready(function () {
    $(".cssbtn").click(function () {
        $(".cssp").css({ "background-color": "grey", "font-size": "200%" });
    });
});

// Dimensions
$(document).ready(function () {
    $(".simpleDimensions").click(function () {
        var txt = "";
        txt += "Width: " + $(".box").width() + "</br>";
        txt += "Height: " + $(".box").height();
        $(".box").html(txt);
    });
    $(".innerDimensions").click(function () {
        var txt = "";
        txt += "Width: " + $(".box").innerWidth() + "</br>";
        txt += "Height: " + $(".box").innerHeight();
        $(".box").html(txt);
    });
    $(".outerDimensions").click(function () {
        var txt = "";
        txt += "Width: " + $(".box").outerWidth() + "</br>";
        txt += "Height: " + $(".box").outerHeight();
        $(".box").html(txt);
    });
    $(".trueOuterDimensions").click(function () {
        var txt = "";
        txt += "Width: " + $(".box").outerWidth(true) + "</br>";
        txt += "Height: " + $(".box").outerHeight(true);
        $(".box").html(txt);
    });

    // Display
    $(".displaybtn").click(function () {
        var txt = "";
        txt += "Document width/height: " + $(document).width();
        txt += "x" + $(document).height() + "\n";
        txt += "Window width/height: " + $(window).width();
        txt += "x" + $(window).height();
        alert(txt);
    });
});

// Traversing Up
$(document).ready(function () {
    // $(".parent").parent().css({ "color": "red", "border": "2px solid red" });
    // $(".parents").parent().css({ "color": "red", "border": "2px solid red" });
    // $(".parents").parents("ul").css({ "color": "green", "border": "2px solid green" });
});

// Traversing Down
$(document).ready(function () {
    $(".descendants").children().css({ "color": "blue", "border": "2px solid blue" });
    $(".descendantsfind").find("span").css({ "color": "blue", "border": "2px solid blue" });
});

// Flitering
$(document).ready(function () {
    $(".firstFliter").first().css("background-color", "peru");
    $(".firstFliter").last().css("background-color", "coral");
    $(".firstFliter").eq(2).css("background-color", "chocolate");
    $(".firstFliter p").filter(".intro").css("background-color", "burlywood");
    $(".firstFliter p").not(".intro").css("background-color", "moccasin");
});

//Practice
$(document).ready(function() {
    $(".yellow").click(function() {
        $(".yellow").filter(this).css("background-color", "yellow");
        $(".yellow").not(this).css("background-color", "")
    });
});